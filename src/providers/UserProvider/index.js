import React from 'react'
import PropTypes from 'prop-types'

const UserContext = React.createContext()

const UserConsumer = UserContext.Consumer

class UserProvider extends React.Component {
  state = {
    isAuthenticated: false,
    currentUser: {
      _id: null,
      name: null,
      points: null,
      __v: null,
      redeemHistory: null,
      createDate: null
    },
    setCurrentUser: (currentUser) => {
      this.setState({
        isAuthenticated: true,
        currentUser: { ...currentUser }
      })
    }
  }

  render() {
    const context = { ...this.state }
    return (
      <UserContext.Provider value={{ user: context }}>
        {this.props.children}
      </UserContext.Provider>
    )
  }
}

UserProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.object,
    PropTypes.node
  ]).isRequired,
}

export {
  UserConsumer,
  UserProvider
}