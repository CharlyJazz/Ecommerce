import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { updateObject } from '../../shared/updateObject';
import { redeemAProduct } from '../../api_client/endpoints/redeem';

const EcommerceContext = React.createContext()

const EcommerceConsumer = EcommerceContext.Consumer

class EcommerceProvider extends React.Component {
  state = {
    products: null, // -> [{}, {}, {}, ..., {}]
    categories: null, // -> // -> [{name: ..., length: ...}, {}, {}, ..., {}]
    criteria: {
      textSearch: '',
      category: 'all',
      upToPoints: 1500,
      showOnlyRedeem: false,
      sortBy: 'lowerPoints',
      sortOptions: [
        {
          value: 'lowerPoints',
          displayText: 'Lowest first'
        },
        {
          value: 'higherPoints',
          displayText: 'Higher first'
        }
      ]
    },
    /*
    |--------------------------------------------------
    | 1.) If if exist criteria to filter
    | 2.) Filter or not the products
    | 3.) Check if the already buyed this a product and add to the product a flag
    | 4.) Check if the @points of the user not are enough to a product and add to the product a flag
    | 5.) Return products filtered or all
    |--------------------------------------------------
    */
    getProducts: (userPoints) => {
      const {
        products,
        criteria: {
          textSearch,
          category,
          upToPoints,
          showOnlyRedeem,
          sortBy
        }
      } = this.state

      return _.chain(products)
        .filter(category === 'all' ? () => true : product => product.category === category)
        .filter(textSearch.length === 0 ? () => true : product => product.name.toLowerCase().indexOf(textSearch.toLowerCase()) > - 1)
        .filter(product => product.cost <= upToPoints)
        .filter(showOnlyRedeem === true ? product => product.cost <= userPoints : () => true)
        .orderBy(['cost'], [sortBy === 'lowerPoints' ? 'asc' : 'desc'])
        .value();
    },
    /**
    |--------------------------------------------------
    | 1.) Calculate categories state and set it
    |--------------------------------------------------
    */
    setProducts: (products) => {
      const categories = _.uniq(_.map(products, 'category'))
      this.setState({
        products: [...products],
        categories: categories,
      })
    },
    /**
    |--------------------------------------------------
    | 1.) Change criteria to filter
    |--------------------------------------------------
    */
    setCriteria: (newCriteria) => { // {category: 'Phone'}
      const { criteria } = this.state
      const updatedCriteria = updateObject(criteria, newCriteria)
      this.setState({
        criteria: updatedCriteria
      })
    },
    /**
    |--------------------------------------------------
    | Redeem a product
    | 1.) Notification
    |--------------------------------------------------
    */
    redeemProduct: (productId) => {
      return redeemAProduct(productId)
    }
  }

  render() {
    const context = { ...this.state }
    return (
      <EcommerceContext.Provider value={{ ecommerce: context }}>
        {this.props.children}
      </EcommerceContext.Provider>
    )
  }
}

EcommerceProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.object,
  ]).isRequired,
}

export {
  EcommerceConsumer,
  EcommerceProvider
}