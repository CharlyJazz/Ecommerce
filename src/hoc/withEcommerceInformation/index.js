import React from 'react'
import { EcommerceConsumer } from '../../providers/EcommerceProvider'

export default (WrappedComponent) => {
  const withEcommerceInformation = props => (
    <EcommerceConsumer>
      {value => (
        <WrappedComponent
          {...props}
          {...value}
        />
      )}
    </EcommerceConsumer>
  )

  return withEcommerceInformation
}