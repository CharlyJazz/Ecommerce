import React, { Component } from 'react'
import Loader from '../../components/Loader';
import FilterBar from '../../components/FilterBar';
import Banner from '../../components/Banner';
import EcommerceCards from '../../components/EcommerceCards';
import withUserInformation from '../../hoc/withUserInformation';
import EcommerceLayout from '../../layout/EcommerceLayout';
import { getUserInformation } from '../../api_client/endpoints/user';
import withEcommerceInformation from '../../hoc/withEcommerceInformation';
import FlexCenter from '../../components/FlexCenter';
import { getListOfProducts } from '../../api_client/endpoints/products';

class Home extends Component {
  componentDidMount = () => {
    const { user, ecommerce } = this.props

    getUserInformation()
      .then(res => {
        user.setCurrentUser(res)
        setTimeout(() => {
          this.setState({ loading: false })
          getListOfProducts()
            .then(res => {
              ecommerce.setProducts(res)
            })
        }, 1500);
      })
  }

  render() {
    const { isAuthenticated, currentUser } = this.props.user
    const { products } = this.props.ecommerce

    let componentRender = <Loader />

    if (isAuthenticated) {
      componentRender = (
        <EcommerceLayout {...currentUser}>
          <Banner />
          <FilterBar />
          {
            products
              ? (
                <EcommerceCards />
              )
              : (
                <FlexCenter height='40vh'>
                  <Loader noAbsolute={true} />
                </FlexCenter>
              )
          }
        </EcommerceLayout>
      )
    }

    return (
      <React.Fragment>
        {componentRender}
      </React.Fragment>
    )
  }
}

export default withEcommerceInformation(withUserInformation(Home))