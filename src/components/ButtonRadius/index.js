import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.module.css'

const ButtonRadius = ({ children, ...props }) => {
  return (
    <button
      className={styles.Button}
      {...props}
    >
      {children}
    </button>
  )
}

ButtonRadius.propTypes = {
  children: PropTypes.node.isRequired
}

export default ButtonRadius