import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.module.css'

const Wrapper = ({ children, debug }) => {
  const classes = [styles.Wrapper]

  debug && classes.push(styles.Debug)

  return (
    <div className={classes.join(' ')}>
      {children}
    </div>
  )
}

Wrapper.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Wrapper