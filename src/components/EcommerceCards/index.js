import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from './styles.module.css'
import productsModel from '../../models/products';
import Wrapper from '../Wrapper';
import { Row, Col } from 'react-grid-system';
import Card from '../Card';
import Margin from '../Margin';
import withEcommerceInformation from '../../hoc/withEcommerceInformation';
import withUserInformation from '../../hoc/withUserInformation';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

class EcommerceCards extends Component {
  state = {
    ready: false
  }

  static propTypes = {
    ecommerce: PropTypes.shape({
      products: PropTypes.arrayOf(productsModel).isRequired
    }).isRequired,
  }

  componentDidMount = () => {
    const { ecommerce: { products } } = this.props

    products.length && this.setState({ ready: true })
  }

  render() {
    const {
      ecommerce: {
        getProducts
      },
      user: {
        currentUser: {
          points
        }
      }
    } = this.props // Replace that with getProducts to allow filtering
    const { ready } = this.state
    return (
      <div className={styles.EcommerceCards}>
        <Wrapper>
          <Row style={{ margin: 0 }}>
            <TransitionGroup component={null}>
              {
                ready && getProducts(points).map((n, i) => (
                  <CSSTransition
                    key={n._id}
                    timeout={400}
                    classNames={{
                      appear: '',
                      appearActive: '',
                      enter: styles['EcommerceCardS-Item-enter'],
                      enterActive: styles['EcommerceCardS-Item-enter-active'],
                      enterDone: '',
                      exit: styles['EcommerceCardS-Item-exit'],
                      exitActive: styles['EcommerceCardS-Item-active'],
                      exitDone: '',
                    }}
                  >
                    <Margin key={n._id}>
                      <Col xs={12} sm={6} md={4} lg={3}>
                        <Card {...n} />
                      </Col>
                    </Margin>
                  </CSSTransition>
                ))
              }
            </TransitionGroup>
          </Row>
        </Wrapper>
      </div>
    )
  }
}

export default withEcommerceInformation(withUserInformation(EcommerceCards))