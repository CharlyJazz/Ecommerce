import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.module.css'
import Wrapper from '../Wrapper';
import avatar from '../../assets/images/avatar.png'
import CoinIcon from '../CoinIcon';
import withEcommerceInformation from '../../hoc/withEcommerceInformation';
import withUserInformation from '../../hoc/withUserInformation';

const InputHeader = ({ onChange, value, label }) => (
  <React.Fragment>
    <input
      value={value}
      className={styles.Input}
      placeholder='Ready? Search for what you want'
      id={label}
      onChange={event => onChange(event.target.value)}
    />
    <label className={styles.Label} htmlFor={label}>
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect width="24" height="24" fill="black" fillOpacity="0" />
        <path d="M15.5 14H14.71L14.43 13.73C15.41 12.59 16 11.11 16 9.5C16 5.91 13.09 3 9.5 3C5.91 3 3 5.91 3 9.5C3 13.09 5.91 16 9.5 16C11.11 16 12.59 15.41 13.73 14.43L14 14.71V15.5L19 20.49L20.49 19L15.5 14ZM9.5 14C7.01 14 5 11.99 5 9.5C5 7.01 7.01 5 9.5 5C11.99 5 14 7.01 14 9.5C14 11.99 11.99 14 9.5 14Z" fill="#2D9CDB" />
      </svg>
    </label>
  </React.Fragment>
)

const Header = ({
  ecommerce: {
    setCriteria,
    criteria: {
      textSearch
    }
  },
  user: {
    currentUser: {
      name,
      points
    }
  }
}) => {
  return (
    <Wrapper>
      <header className={styles.Header}>
        <div>
          <span>
            <svg width="39" height="36" viewBox="0 0 39 36" fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect width="38.7692" height="36" fill="black" fillOpacity="0" />
              <rect width="38.7692" height="36" fill="black" fillOpacity="0" />
              <rect width="38.7692" height="36" fill="black" fillOpacity="0" />
              <rect width="38.7692" height="36" fill="black" fillOpacity="0" />
              <path fillRule="evenodd" clipRule="evenodd" d="M30.8682 0.404545C30.6406 0.0177288 30.1363 -0.114895 29.7422 0.108532L16.2482 7.75465C15.9412 7.92879 15.7817 8.27648 15.8529 8.61759L20.3943 30.3988C20.4161 30.5037 20.3708 30.6586 20.3021 30.7346L19.7503 31.3454C18.6032 32.6151 17.6315 33.303 15.7844 33.303C13.7134 33.303 12.741 32.2316 11.2013 30.3378C9.36249 28.0764 7.07444 25.262 1.51136 25.262H1.3738C0.615076 25.262 0 25.8657 0 26.6104C0 27.355 0.615076 27.9587 1.3738 27.9587H1.51136C5.75146 27.9587 7.35535 29.9313 9.05358 32.0198C10.5704 33.8855 12.2897 36 15.7844 36C18.8014 36 20.4616 34.6227 21.8061 33.1346L26.7657 27.6457C26.7657 27.6454 38.5617 14.5895 38.5617 14.5895C38.797 14.329 38.8362 13.95 38.6588 13.6483L30.8682 0.404545Z" fill="#FF6600" />
            </svg>
          </span>
        </div>
        <div>
          <span>
            <div className={styles.InputField}>
              <InputHeader
                label='desktop__search__input'
                onChange={value => setCriteria({ textSearch: value })}
                value={textSearch}
              />
            </div>
          </span>
        </div>
        <div>
          <span>
            <div className={styles.BlockUserData}>
              <div>
                <span>
                  <CoinIcon />
                </span>
                <span className={styles.SpanPoints}>
                  {points}
                </span>
              </div>
              <div>
                <span className={styles.SpanName}>
                  Hi {name}!
                </span>
                <img
                  src={avatar}
                  alt='Avatar'
                  className={styles.Avatar}
                />
              </div>
            </div>
          </span>
        </div>
      </header>
      <div className={[styles.InputField, styles.InputFieldMobile].join(' ')}>
        <InputHeader
          label='mobile_search_input'
          onChange={value => setCriteria({ textSearch: value })}
          value={textSearch}
        />
      </div>
    </Wrapper >
  )
}

Header.propTypes = {
  ecommerce: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
}

export default withEcommerceInformation(withUserInformation(Header))