import React from 'react'
import PropTypes from 'prop-types'
import { ScreenClassRender } from 'react-grid-system'

const orientationAllows = ['right', 'left', 'bottom', 'top']

const Margin = ({ children, orientation, breakpoints }) => {
  if (orientationAllows.includes(orientation)) {
    const orientationMargin = `margin${orientation.replace(/\w/, c => c.toUpperCase())}`
    return (
      <ScreenClassRender render={screenClass => {
        const margin = breakpoints.includes(screenClass) ? '2rem' : null // ['xs', 'sm', 'md']
        const chidrenModified = React.cloneElement(children, {
          ...(margin ? {
            style: {
              [orientationMargin]: margin
            }
          } : null)
        })
        return chidrenModified
      }} />
    )
  }
  else {
    console.warn("[Margin Component]: Need orientation -> ['right', 'left', 'bottom', 'top'] ")
    return null
  }
}

Margin.defaultProps = {
  breakpoints: ['xs', 'sm', 'md', 'lg', 'xl'],
  orientation: 'bottom'
}

Margin.propTypes = {
  breakpoints: PropTypes.array.isRequired,
  orientation: PropTypes.oneOf(orientationAllows),
  children: PropTypes.node.isRequired
}

export default Margin