import React from 'react'
import PropTypes from 'prop-types'

const FlexCenter = ({ children, height }) => {
  return (
    <div style={{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: height
    }}>
      {children}
    </div>
  )
}

FlexCenter.defaultProps = {
  height: '100%'
}

FlexCenter.propTypes = {
  children: PropTypes.node.isRequired,
  height: PropTypes.string.isRequired
}

export default FlexCenter