import React from 'react'
import { ClipLoader } from 'react-spinners';
import styles from './styles.module.css'

export const Spinner = () => (
  <ClipLoader
    color={'#2D9CDB'}
    loading={true}
    size={100}
  />
)

export default ({ noAbsolute }) => {
  return (
    <div className={[noAbsolute ? styles.noAbsolute : styles.Loader].join(' ')}>
      {
        noAbsolute ? <Spinner /> : (
          <div>
            <svg className={styles.Logo} width="39" height="36" viewBox="0 0 39 36" fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect width="38.7692" height="36" fill="black" fillOpacity="0" />
              <rect width="38.7692" height="36" fill="black" fillOpacity="0" />
              <rect width="38.7692" height="36" fill="black" fillOpacity="0" />
              <rect width="38.7692" height="36" fill="black" fillOpacity="0" />
              <path fillRule="evenodd" clipRule="evenodd" d="M30.8682 0.404545C30.6406 0.0177288 30.1363 -0.114895 29.7422 0.108532L16.2482 7.75465C15.9412 7.92879 15.7817 8.27648 15.8529 8.61759L20.3943 30.3988C20.4161 30.5037 20.3708 30.6586 20.3021 30.7346L19.7503 31.3454C18.6032 32.6151 17.6315 33.303 15.7844 33.303C13.7134 33.303 12.741 32.2316 11.2013 30.3378C9.36249 28.0764 7.07444 25.262 1.51136 25.262H1.3738C0.615076 25.262 0 25.8657 0 26.6104C0 27.355 0.615076 27.9587 1.3738 27.9587H1.51136C5.75146 27.9587 7.35535 29.9313 9.05358 32.0198C10.5704 33.8855 12.2897 36 15.7844 36C18.8014 36 20.4616 34.6227 21.8061 33.1346L26.7657 27.6457C26.7657 27.6454 38.5617 14.5895 38.5617 14.5895C38.797 14.329 38.8362 13.95 38.6588 13.6483L30.8682 0.504545Z" fill="#FF6600" />
            </svg>
            <Spinner />
          </div>
        )
      }
    </div>
  )
}