import React from 'react'
import { Slider as SliderCompound, Handles, Tracks } from 'react-compound-slider'
import PropTypes from 'prop-types'
import styles from './styles.module.css'

const Track = ({ source, target, getTrackProps }) => {
  return (
    <div
      className={styles.Track}
      style={{
        left: `${source.percent}%`,
        width: `${target.percent - source.percent}%`,
      }}
      {...getTrackProps()}
    />
  )
}

const Handle = ({
  handle: {
    id,
    value,
    percent
  },
  getHandleProps,
}) => {
  return (
    <div
      className={styles.Handle}
      style={{ left: `${percent}%` }}
      {...getHandleProps(id)}
    >
    </div>
  )
}

const Slider = ({ onChange, value }) => {
  return (
    <SliderCompound
      className={styles.Slider}
      domain={[5, 1500]}
      mode={1}
      step={1}
      values={[value]}
      onChange={(values) => onChange(values[0])}
    >
      <div className={styles.Rail} />
      <Handles>
        {({ handles, getHandleProps }) => (
          <div className="slider-handles">
            {handles.map(handle => (
              <Handle
                key={handle.id}
                handle={handle}
                getHandleProps={getHandleProps}
              />
            ))}
          </div>
        )}
      </Handles>
      <Tracks left={false}>
        {({ tracks, getTrackProps }) => (
          <div className="slider-tracks">
            {tracks.map(({ id, source, target }) => (
              <Track
                key={id}
                source={source}
                target={target}
                getTrackProps={getTrackProps}
              />
            ))}
          </div>
        )}
      </Tracks>
    </SliderCompound>
  )
}

Slider.propTypes = {
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired
}

export default Slider