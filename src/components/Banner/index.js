import React from 'react'
import styles from './styles.module.css'
import Wrapper from '../Wrapper';
import bannerImage from '../../assets/images/banner.png'

const Banner = () => {
  return (
    <div className={styles.Banner}>
      <Wrapper>
        <div>
          <div>
            <span>The highest technologies for the<br /><strong> best customers</strong></span>
          </div>
          <div>
            <div>
              <img
                src={bannerImage}
                alt='Products in the ecommerce'
              />
            </div>
          </div>
        </div>
      </Wrapper>
    </div>
  )
}

export default Banner