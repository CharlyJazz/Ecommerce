import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.module.css'
import Wrapper from '../Wrapper';
import Slider from '../Slider';
import withEcommerceInformation from '../../hoc/withEcommerceInformation';

const FilterBar = ({
  ecommerce: {
    setCriteria,
    categories,
    criteria: {
      category,
      upToPoints,
      showOnlyRedeem,
      sortOptions,
      sortBy
    }
  }
}) => {
  return (
    <div className={styles.FilterBar}>
      <Wrapper>
        <div className={styles.FilterBarContent}>
          <div>
            <div className={styles.FilterBy}>
              <span>
                Filters
              </span>
            </div>
          </div>
          <div>
            <div className={styles.FilterBy}>
              <div>
                {
                  categories ? (
                    <select value={category} onChange={(event) => setCriteria({ category: event.target.value })}>
                      <option value='all'>All categories</option>
                      {
                        categories.map(n => <option value={n} key={n}>{n}</option>)
                      }
                    </select>
                  ) : null
                }
              </div>
            </div>
            <div className={styles.Range}>
              <span>
                Up to {upToPoints} points
              </span>
              <div>
                <Slider
                  value={upToPoints}
                  onChange={(value) => setCriteria({ upToPoints: value })}
                />
              </div>
            </div>
            <div className={styles.OnlyRedeem}>
              {
                showOnlyRedeem
                  ? (
                    <svg onClick={() => setCriteria({ showOnlyRedeem: false })} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <rect width="24" height="24" fill="black" fillOpacity="0" />
                      <path d="M19 3H5C3.89 3 3 3.9 3 5V19C3 20.1 3.89 21 5 21H19C20.11 21 21 20.1 21 19V5C21 3.9 20.11 3 19 3ZM10 17L5 12L6.41 10.59L10 14.17L17.59 6.58L19 8L10 17Z" fill="#A0A0A0" />
                    </svg>
                  )
                  : (
                    <svg onClick={() => setCriteria({ showOnlyRedeem: true })} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                      <path d="M19 5v14H5V5h14m0-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z" fill="#A0A0A0" />
                      <path d="M0 0h24v24H0z" fill="none" />
                    </svg>
                  )
              }
              <span>Show only what I can redeem</span>
            </div>
          </div>
          <div>
            <div className={[styles.FilterBy, styles.SmallSelect].join(' ')}>
              <span>
                Sort by
              </span>
              <div>
                <select value={sortBy} onChange={(event) => setCriteria({ sortBy: event.target.value })} >
                  {
                    sortOptions.map(n => <option value={n.value} key={n.value}>{n.displayText}</option>)
                  }
                </select>
              </div>
            </div>
          </div>
        </div>
      </Wrapper>
    </div >
  )
}

FilterBar.propTypes = {
  categories: PropTypes.array
}

export default withEcommerceInformation(FilterBar)