import React from 'react'
import styles from './styles.module.css'
import productsModel from '../../models/products';
import ButtonRadius from '../ButtonRadius';
import withEcommerceInformation from '../../hoc/withEcommerceInformation';
import withUserInformation from '../../hoc/withUserInformation';
import CoinIcon from '../CoinIcon';
import { getUserInformation } from '../../api_client/endpoints/user';
import { toast } from 'react-toastify';

const Card = ({
    _id,
    cost,
    category,
    name,
    img,
    ecommerce: {
        setCriteria,
        redeemProduct
    },
    user: {
        currentUser: {
            points
        },
        setCurrentUser
    }
}) => {
    return (
        <div className={styles.Card}>
            <p className={styles.Category} onClick={() => setCriteria({ category: category })}>
                {category}
            </p>
            <img
                src={img.url}
                alt={`${name} ${category}`}
                className={styles.Img}
            />
            <p className={styles.Name}>
                {name}
            </p>
            <p className={styles.Cost}>
                {cost}
                <span>
                    <CoinIcon />
                </span>
            </p>
            {
                cost > points
                    ? (
                        <p className={styles.NoAllow}>
                            You need {cost - points} points
                        </p>
                    )
                    : (
                        <ButtonRadius onClick={
                            () => redeemProduct(_id)
                                .then(res => {
                                    toast(res.message.replace('product', name), {
                                        autoClose: 2000,
                                        type: 'info',
                                        position: toast.POSITION.BOTTOM_CENTER,
                                        pauseOnHover: false,
                                        className: styles.Toast,
                                        bodyClassName: styles.ToastBody
                                    })
                                    getUserInformation()
                                        .then(res => {
                                            setCurrentUser(res)
                                        })
                                })
                        }>
                            Redeem Now
                        </ButtonRadius>
                    )
            }
        </div>
    )
}

Card.propTypes = productsModel

export default withEcommerceInformation(withUserInformation(Card))