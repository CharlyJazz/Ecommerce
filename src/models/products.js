import PropTypes from 'prop-types'

/**
|--------------------------------------------------
| Useful for no repeat proptypes schema definitions
|--------------------------------------------------
*/
const productsModel = PropTypes.shape({
  _id: PropTypes.string.isRequired,
  cost: PropTypes.number.isRequired,
  category: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  img: PropTypes.shape({
    hdUrl: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
  })
}).isRequired

export default productsModel