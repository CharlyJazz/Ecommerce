import React from 'react'
import PropTypes from 'prop-types'
import Header from '../../components/Header';
import { ToastContainer } from 'react-toastify';
import Footer from '../../components/Footer';
import 'react-toastify/dist/ReactToastify.css';

const EcommerceLayout = ({ children }) => (
  <React.Fragment>
    <Header />
    <main>
      {children}
    </main>
    <Footer />
    <ToastContainer />
  </React.Fragment>
)

EcommerceLayout.propTypes = {
  children: PropTypes.node.isRequired
}

export default EcommerceLayout