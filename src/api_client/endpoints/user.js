import { post, get } from '../core';

const NAMESPACE = '/user';
const POINTS_ALLOWED = [1000, 5000, 7500]

/**
|--------------------------------------------------
| Get personal information of the authenticated user
|--------------------------------------------------
*/
export function getUserInformation() {
  return get(NAMESPACE + '/me');
}

/**
|--------------------------------------------------
|  Get user redeem history. 
|
|  Take into account that if you didn’t redeem any product,
|  you’ll get an empty array as a response.
|--------------------------------------------------
*/
export function getUserHistory() {
  return get(NAMESPACE + '/history');
}

/**
|--------------------------------------------------
|  Set points to the authenticated user
|--------------------------------------------------
*/
export function setUserPoints(points) {
  if (POINTS_ALLOWED.includes(points)) {
    return post(NAMESPACE + '/points', {
      'amount': points
    });
  } else {
    console.warn('You can only add them by 1000, 5000 or 7500 in the body request. Any other value will be rejected.\nSee https://aerolabchallenge.docs.apiary.io/#reference/0/points')
  }
}