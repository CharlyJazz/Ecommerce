import { get } from '../core';

const NAMESPACE = '/products';

/**
|--------------------------------------------------
| Get list of products
|--------------------------------------------------
*/
export function getListOfProducts() {
  return get(NAMESPACE);
}