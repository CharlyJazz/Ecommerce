import { post } from '../core';

const NAMESPACE = '/redeem';

/**
|--------------------------------------------------
| Claim a product and add it to the user’s redeem history.
| Take into account that if your user doesn't have the
| required points, you won't be able to redeem the product.
| Inside the body request, the product Id is expected.
|--------------------------------------------------
*/

export function redeemAProduct(productId) {
  return post(NAMESPACE, {
    'productId': productId
  });
}