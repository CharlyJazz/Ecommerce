import axios from 'axios';

/**
|--------------------------------------------------
| Configuration Metadata
|--------------------------------------------------
*/
const APY_KEY = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YjczNTNlZjQ4ODVhMzAwNTg3MzYxMGQiLCJpYXQiOjE1MzQyODQ3ODN9.ONqaZTjzTHXU1YvgUg2WmppmbzUTDSNTcUK-4QQNoro'
const CONFIG = {
  responseType: 'json',
  headers: { 'Authorization': `Bearer ${APY_KEY}` }
}
const URL_SERVER_API = 'https://aerolab-challenge.now.sh'

/**
|--------------------------------------------------
| Create POST request with axios
| See endpoints/ folder
|--------------------------------------------------
*/
export function post(path, bodyParameters = {}, configExtra = {}) {
  return axios.post(
    URL_SERVER_API + path,
    bodyParameters,
    Object.assign({}, CONFIG, configExtra)
  ).then(res => {
    return res.data;
  });
}

/**
|--------------------------------------------------
| Create GET request with axios
| See endpoints/ folder
|--------------------------------------------------
*/
export function get(path) {
  return axios.get(
    URL_SERVER_API + path,
    CONFIG
  ).then(res => {
    return res.data;
  });
}