import App from './applications'
import React from 'react'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import MobileDetect from 'mobile-detect'
import express from 'express'

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST);

const server = express();
server
  .disable('x-powered-by')
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
  .get('/*', (req, res) => {
    const md = new MobileDetect(req.headers['user-agent']);
    let defaultScreenClass = 'xl';
    if (md.phone() !== null) defaultScreenClass = 'xs';
    if (md.tablet() !== null) defaultScreenClass = 'md';
    const context = {};
    const markup = renderToString(
      <StaticRouter context={context} location={req.url}>
        <App defaultScreenClass={defaultScreenClass} />
      </StaticRouter>
    );

    if (context.url) {
      res.redirect(context.url);
    } else {
      res.status(200).send(
        `<!doctype html>
          <html lang="en">
          <head>
              <meta http-equiv="X-UA-Compatible" content="IE=edge" />
              <meta charset="utf-8" />
              <title>Ecommerce</title>
              <meta name="description" content="Aerolab Challenge by Charlyjazz">
              <meta name="keywords" content="Aerolab,Ecommerce,React,Lodash,PWA">
              <meta name="author" content="Charlyjazz">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png">
              <link rel="apple-touch-icon" sizes="60x60" href="apple-icon-60x60.png">
              <link rel="apple-touch-icon" sizes="72x72" href="apple-icon-72x72.png">
              <link rel="apple-touch-icon" sizes="76x76" href="apple-icon-76x76.png">
              <link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png">
              <link rel="apple-touch-icon" sizes="120x120" href="apple-icon-120x120.png">
              <link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png">
              <link rel="apple-touch-icon" sizes="152x152" href="apple-icon-152x152.png">
              <link rel="apple-touch-icon" sizes="180x180" href="apple-icon-180x180.png">
              <link rel="icon" type="image/png" sizes="192x192"  href="android-icon-192x192.png">
              <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
              <link rel="icon" type="image/png" sizes="96x96" href="favicon-96x96.png">
              <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
              <link rel="manifest" href="manifest.json">
              <meta name="msapplication-TileColor" content="#f60">
              <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
              <meta name="theme-color" content="#f60">
              ${
        assets.client.css
          ? `<link rel="stylesheet" href="${assets.client.css}">`
          : ''
        }
              ${
        process.env.NODE_ENV === 'production'
          ? `<script src="${assets.client.js}" defer></script>`
          : `<script src="${assets.client.js}" defer crossorigin></script>`
        }
          </head>
          <body>
              <div id="root">${markup}</div>
              <noscript>
                You need to enable JavaScript to run this app.
              </noscript>
          </body>
        ${process.env.NODE_ENV === 'production'
          ? `<script>
                if ('serviceWorker' in navigator) {
                  window.addEventListener('load', function() {
                    navigator.serviceWorker.register('/service-worker.js');
                  });
                }
              </script>`
          : ''
        }
        </html>`
      );
    }
  });

export default server;
